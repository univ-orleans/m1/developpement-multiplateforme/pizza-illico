﻿using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Pizzas;
using PizzaIllico.Mobile.Services;
using PizzaIllico.Mobile.ViewModels;
using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace PizzaIllico.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ShopMapPage
    {
        private Map map1;
        public ShopMapPage()
        {
            InitializeComponent();
            map1 = new Map();
            InitPosition();
            Content = map1;
        }
        public async void InitPosition()
        {
            var locator = CrossGeolocator.Current;
            var position = await locator.GetPositionAsync();
            map1.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(position.Latitude, position.Longitude),
                                                         Distance.FromMiles(1)));
        }
        public async void getShops()
        {
            IPizzaApiService service = DependencyService.Get<IPizzaApiService>();

            Response<List<ShopItem>> response = await service.ListShops();

            Console.WriteLine($"Appel HTTP: {response.IsSuccess}");
            if (response.IsSuccess)
            {
                Console.WriteLine($"Appel HTTP: {response.Data.Count}");
                List<ShopItem> shops = new List<ShopItem>(response.Data);

                // Enregistrer le pin sur le map 
                for (int index = 0; index < shops.Count(); index++)
                {
                    ShopItem shop = shops[index];
                    Pin pin = new Pin
                    {
                        Label = shop.Name,
                        Address = shop.Address,
                        Type = PinType.Place,
                        Position = new Position(shop.Latitude, shop.Longitude)
                    };
                    map1.Pins.Add(pin);

                }
            }
        }
        protected override void OnAppearing()
        {
            getShops();

        }
    }

}
