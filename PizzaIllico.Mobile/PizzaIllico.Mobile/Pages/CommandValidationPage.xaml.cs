﻿using PizzaIllico.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PizzaIllico.Mobile.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CommandValidationPage : ContentPage
    {
        // CONSTRUCTOR --------------------------------------------------------
        public CommandValidationPage()
        {
            InitializeComponent();
            BindingContext = new CommandValidationViewModel();
        }



        // EVENEMENTS ---------------------------------------------------------
        protected override void OnAppearing()
        {
            if (BindingContext is CommandValidationViewModel viewModel) { viewModel.Update(); }
        }
    }
}