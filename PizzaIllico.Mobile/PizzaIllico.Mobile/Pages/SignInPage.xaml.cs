﻿using PizzaIllico.Mobile.Dtos.Authentications.Credentials;
using PizzaIllico.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PizzaIllico.Mobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SignInPage : ContentPage
	{
		// PROPERTIES : PRIVATE -----------------------------------------------------
		private readonly SignInViewModel viewModel;


		// CONSTRUCTOR --------------------------------------------------------------
		public SignInPage()
		{
			InitializeComponent();
			viewModel = new SignInViewModel();
			BindingContext = viewModel;
		}


		// EVENEMENTS ---------------------------------------------------------
		private void SaveForm(object sender, EventArgs e)
		{
			// création de l'utilisateur
			LoginWithCredentialsRequest loginRequest = new LoginWithCredentialsRequest();
			loginRequest.ClientId = "MOBILE";
			loginRequest.ClientSecret = "UNIV";
			loginRequest.Login= email.Text;
			loginRequest.Password = password.Text;

			// Exécution de la méthode de d'authentification d'un utilisateur
			viewModel.AuthentifyUser(loginRequest);
		}
	}
}