﻿using PizzaIllico.Mobile.Dtos.Authentications.Credentials;
using PizzaIllico.Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PizzaIllico.Mobile.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class UserSettingsPage : ContentPage
	{

		// ATTRIBUTES ---------------------------------------------------------



		// CONSTRUCTOR --------------------------------------------------------
		public UserSettingsPage()
		{   
			InitializeComponent();
			BindingContext = new UserSettingsViewModel();
		}



		// EVENTS -------------------------------------------------------------
		protected override void OnAppearing()
		{
			if (BindingContext is UserSettingsViewModel viewModel) { viewModel.GetUer(); }
		}
	}
}