﻿using PizzaIllico.Mobile.Dtos.Authentications;
using PizzaIllico.Mobile.Dtos.Authentications.Credentials;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;

namespace PizzaIllico.Mobile.Services
{
	interface IOAuth2Service
    {
		// GET
		LoginResponse LoginResponse();
		string AccessToken();
		string RefreshToken();
		string TokenType();
		int ExpiresIn();
		bool IsExpired();
		bool CheckExpired();
		RefreshRequest getRefreshRequest();

		// SET
		void SaveToken(LoginResponse loginResponse);
	}




	class OAuth2Service : IOAuth2Service
	{
		// PROPERTIES : PRIVATE -----------------------------------------------
		private static string PREFERENCES_ROOT = "oauth2_";
		private static string PREFERENCES_ACCESS_TOKEN  = PREFERENCES_ROOT + "access_token";
		private static string PREFERENCES_REFRESH_TOKEN = PREFERENCES_ROOT + "refresh_token";
		private static string PREFERENCES_TOKEN_TYPE    = PREFERENCES_ROOT + "token_type";
		private static string PREFERENCES_USAGE_LIMIT   = PREFERENCES_ROOT + "usage_limit";

		private LoginResponse _loginResponse;
		private DateTime _usageLimit;



		// CONSTRUCTOR --------------------------------------------------------
		public OAuth2Service()
		{
			_loginResponse = new LoginResponse();
			_loginResponse.AccessToken = Preferences.Get(PREFERENCES_ACCESS_TOKEN, PREFERENCES_ACCESS_TOKEN);
			_loginResponse.RefreshToken = Preferences.Get(PREFERENCES_REFRESH_TOKEN, PREFERENCES_REFRESH_TOKEN);
			_loginResponse.TokenType = Preferences.Get(PREFERENCES_TOKEN_TYPE, PREFERENCES_TOKEN_TYPE);
			_usageLimit = Preferences.Get(PREFERENCES_USAGE_LIMIT, DateTime.Now);
			
		}



		// FUNCTIONS : GET ----------------------------------------------------
		public LoginResponse LoginResponse()
        {
			return _loginResponse;
        }


		public string AccessToken()
		{
			return _loginResponse.AccessToken;
		}


		public string RefreshToken()
		{
			return _loginResponse.RefreshToken;
		}


		public string TokenType()
		{
			return _loginResponse.TokenType;
		}

		public int ExpiresIn()
		{
			return _loginResponse.ExpiresIn;
		}


		public bool IsExpired()
		{
			if (_usageLimit == null)
            {
				return true;
            }
			return DateTime.Now > _usageLimit;
		}

		public bool CheckExpired()
		{
			if (_usageLimit != null)
            {
				int diff = (_usageLimit - DateTime.Now).Hours;

				if (diff == 0)
				{
					return true;
				}
				return false;
			}
			return false;
		}

		public RefreshRequest getRefreshRequest() {
			RefreshRequest refreshRequest = new RefreshRequest();
			refreshRequest.ClientId = "MOBILE";
			refreshRequest.ClientSecret = "UNIV";
			refreshRequest.RefreshToken = _loginResponse.RefreshToken;
			return refreshRequest;
		}

		// FUNCTIONS : SET ----------------------------------------------------
		public void SaveToken(LoginResponse loginResponse)
        {
			_loginResponse = loginResponse;
			_usageLimit = DateTime.Now.AddSeconds(_loginResponse.ExpiresIn);

			// sauvegarde des les shares préférences
			Preferences.Set(PREFERENCES_ACCESS_TOKEN, _loginResponse.AccessToken);
			Preferences.Set(PREFERENCES_REFRESH_TOKEN, _loginResponse.RefreshToken);
			Preferences.Set(PREFERENCES_TOKEN_TYPE, _loginResponse.TokenType);
			Preferences.Set(PREFERENCES_USAGE_LIMIT, _usageLimit);

	}


		// FUNCTIONS : OVERRIDE -----------------------------------------------
		public override string ToString()
        {
            return 
				$"access token: {AccessToken()}\n" +
				$"refresh token: {RefreshToken()}\n" +
				$"token type: {TokenType()}\n" +
				$"expire in: {ExpiresIn()}s\n" +
				$"is expired: {IsExpired()}s\n" +
				$"usage limit: {_usageLimit}s\n"
			;
        }
    }
}
