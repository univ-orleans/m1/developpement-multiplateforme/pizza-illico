﻿using PizzaIllico.Mobile.Dtos.Pizzas;
using System;
using System.Collections.Generic;
using System.Text;

namespace PizzaIllico.Mobile.Services
{
	interface IBasketService
	{
		// GET
		List<ShopItem> Shops();
		List<PizzaItem> Pizzas(ShopItem shop);
		List<PizzaItem> Pizzas();
		Dictionary<ShopItem, List<PizzaItem>> All();

		// ADD
		bool Add(ShopItem shop, PizzaItem pizza);

		// DEL
		bool Remove(ShopItem shop);
		bool Remove(ShopItem shop, PizzaItem pizza);

		// TEST
		bool Contain(ShopItem shop);
		bool Contain(ShopItem shop, PizzaItem pizza);
	}




	sealed class BasketService : IBasketService
	{
		// PROPERTIES : PRIVATE -----------------------------------------------
		private Dictionary<ShopItem, List<PizzaItem>> _basket;



		// PROPERTIES : EXPOSED -----------------------------------------------



		// CONSTRUCTOR --------------------------------------------------------
		public BasketService()
		{
			_basket = new Dictionary<ShopItem, List<PizzaItem>>();
		}



		// FUNCTIONS : GET ----------------------------------------------------
		public List<ShopItem> Shops()
		{
			return new List<ShopItem>(_basket.Keys);
		}


		public List<PizzaItem> Pizzas(ShopItem shop)
		{
			List<PizzaItem> pizzas = new List<PizzaItem>();
			foreach (ShopItem item in Shops())
            {
				if (shop.Id == item.Id)
                {
					_basket.TryGetValue(item, out pizzas);
                }
            }
			return pizzas == null ? new List<PizzaItem>() : pizzas;
		}


		public List<PizzaItem> Pizzas()
		{
			List<PizzaItem> pizzas = new List<PizzaItem>();
			foreach (ShopItem shop in Shops())
            {
				pizzas.AddRange(Pizzas(shop));
            }
			return pizzas;
		}


		public Dictionary<ShopItem, List<PizzaItem>> All()
        {
			return _basket;
        }



		// FUNCTIONS : ADD ----------------------------------------------------
		public bool Add(ShopItem shop, PizzaItem pizza)
		{
			if (Contain(shop))
			{
				List<PizzaItem> pizzas = Pizzas(shop);
				if (Contain(pizzas, pizza))
				{
					return false;
				}
				pizzas.Add(pizza);
				Remove(shop);
				_basket.Add(shop, pizzas);
			}
			else
			{
				List<PizzaItem> pizzas = new List<PizzaItem>();
				pizzas.Add(pizza);
				_basket.Add(shop, pizzas);
			}
			return true;
		}



		// FUNCTIONS : DEL ----------------------------------------------------
		public bool Remove(ShopItem shop)
		{
			foreach (ShopItem item in Shops())
            {
				if (shop.Id == item.Id)
                {
					return _basket.Remove(item);
                }
            }
			return false;
		}


		public bool Remove(ShopItem shop, PizzaItem pizza)
		{
			List<PizzaItem> pizzas = Pizzas(shop);
			bool res = false;
			foreach (PizzaItem item in pizzas)
			{
				if (pizza.Id == item.Id)
				{
					res =pizzas.Remove(item);
					break;
				}
			}
			Remove(shop);
			_basket.Add(shop, pizzas);
			return res;
		}



		// FUNCTIONS : TEST ---------------------------------------------------
		public bool Contain(ShopItem shop)
		{
			foreach (ShopItem item in Shops())
            {
				if (shop.Id == item.Id)
                {
					return true;
                }
            }
			return false;
		}


		public bool Contain(ShopItem shop, PizzaItem pizza)
		{
			if (!Contain(shop))
			{
				return false;
			}
			return Contain(Pizzas(shop), pizza);
		}


		private bool Contain(List<PizzaItem> pizzas, PizzaItem pizza)
        {
			foreach(PizzaItem item in pizzas)
            {
				if (pizza.Id == item.Id)
                {
					return true;
                }
            }
			return false;
        }



		// FUNCTIONS : OVERRIDE -----------------------------------------------
		public override string ToString()
		{
			string renderer = "";
			foreach (ShopItem shop in Shops())
			{
				renderer += $"id({shop.Id}) : {shop.Name}\n";
				foreach (PizzaItem pizza in Pizzas(shop))
				{
					renderer += $"    + id({pizza.Id}) : {pizza.Name}\n";
				}
			}

			return renderer;
		}
	}
}