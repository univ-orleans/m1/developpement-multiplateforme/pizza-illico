using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Accounts;
using PizzaIllico.Mobile.Dtos.Authentications;
using PizzaIllico.Mobile.Dtos.Authentications.Credentials;
using PizzaIllico.Mobile.Dtos.Pizzas;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.Services
{
	public interface IPizzaApiService
	{
		// GET
		Task<Response<List<ShopItem>>> ListShops();
		Task<Response<List<PizzaItem>>> ListPizzas(int shopId);
		Task<Response<UserProfileResponse>> GetUser();
		string GetImagePizza(int shopId, int pizzaId);
		Task<Response<List<OrderItem>>> ListOrders();

		// POST
		Task<Response<LoginResponse>> CreateUser(CreateUserRequest createUserRequest);
		Task<Response<LoginResponse>> AuthentifyUser(LoginWithCredentialsRequest loginRequest);
		Task<Response<OrderItem>> ValidateCommand(long shopId, CreateOrderRequest orderRequest);
		Task<Response<LoginResponse>> Refresh(RefreshRequest refreshRequest);

		// PATCH
		Task<Response<UserProfileResponse>> UpdateUser(SetUserProfileRequest setUserProfileRequest);
		Task<Response> UpdatePassWord(SetPasswordRequest setPasswordRequest);

	}
	
	public class PizzaApiService : IPizzaApiService
	{
		// PROPERTIES : PRIVATE -----------------------------------------------
		private readonly IApiService _apiService;



		// CONSTRUCTOR --------------------------------------------------------
		public PizzaApiService()
		{
			_apiService = DependencyService.Get<IApiService>();
		}



		// REQUEST : GET ------------------------------------------------------
		public async Task<Response<List<ShopItem>>> ListShops()
		{
			return await _apiService.Get<Response<List<ShopItem>>>(Urls.LIST_SHOPS);
		}


		public async Task<Response<List<PizzaItem>>> ListPizzas(int shopId)
		{
			var queryString = new Dictionary<string, int>() {{ "{shopId}", shopId }};
			return await _apiService.Get_Parametre<Response<List<PizzaItem>>>(Urls.LIST_PIZZA, queryString);
		}


		public async Task<Response<UserProfileResponse>> GetUser()
		{
			return await _apiService.Get<Response<UserProfileResponse>>(Urls.USER_PROFILE);
		}

		public string GetImagePizza(int shopId, int pizzaId)
        {
			var queryString = new Dictionary<string, int>() { { "{shopId}", shopId }, { "{pizzaId}", pizzaId} };
			return _apiService.Get_Image_Parametre(Urls.GET_IMAGE, queryString);
        }

		public async Task<Response<List<OrderItem>>> ListOrders()
		{
			return await _apiService.Get<Response<List<OrderItem>>>(Urls.LIST_ORDERS);
		}




		// REQUEST : POST -----------------------------------------------------
		public async Task<Response<LoginResponse>> CreateUser(CreateUserRequest createUserRequest)
		{
			string json = JsonConvert.SerializeObject(createUserRequest);
			return await _apiService.Post<Response<LoginResponse>>(Urls.CREATE_USER,json);
		}


		public async Task<Response<LoginResponse>> AuthentifyUser(LoginWithCredentialsRequest loginRequest)
		{
			string json = JsonConvert.SerializeObject(loginRequest);
			return await _apiService.Post<Response<LoginResponse>>(Urls.LOGIN_WITH_CREDENTIALS, json);
		}

		public async Task<Response<LoginResponse>> Refresh(RefreshRequest refreshRequest)
		{
			string json = JsonConvert.SerializeObject(refreshRequest);
			return await _apiService.Post<Response<LoginResponse>>(Urls.REFRESH_TOKEN,json);
		}


		public async Task<Response<OrderItem>> ValidateCommand(long shopId, CreateOrderRequest orderRequest)
        {
			return await _apiService.Post_Parametre<Response<OrderItem>>(
				Urls.DO_ORDER,
				new Dictionary<string, long>() { { "{shopId}", shopId } },
				JsonConvert.SerializeObject(orderRequest)
			);
		}



		// REQUEST : PATCH ----------------------------------------------------
		public async Task<Response<UserProfileResponse>> UpdateUser(SetUserProfileRequest setUserProfileRequest)
		{
			string json = JsonConvert.SerializeObject(setUserProfileRequest);
			return await _apiService.Patch<Response<UserProfileResponse>>(Urls.SET_USER_PROFILE, json);
		}


		public async Task<Response> UpdatePassWord(SetPasswordRequest setPasswordRequest)
		{
			string json = JsonConvert.SerializeObject(setPasswordRequest);
			return await _apiService.Patch<Response>(Urls.SET_PASSWORD, json);
		}
	}
}