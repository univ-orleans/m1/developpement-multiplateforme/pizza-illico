using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Authentications;
using PizzaIllico.Mobile.Dtos.Pizzas;
using PizzaIllico.Mobile.Pages;
using PizzaIllico.Mobile.Services;
using Plugin.Geolocator;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PizzaIllico.Mobile.ViewModels
{
	public class ShopListViewModel : ViewModelBase
	{
		// PROPERTIES : PRIVATE -----------------------------------------------
		private ObservableCollection<ShopItem> _shops;



		// COMMANDES : DEFINITION ---------------------------------------------
		public ICommand SelectedCommand { get; }
		public ICommand GoUserSettings { get; }
		public ICommand GoCommandValidation { get; }



		// PROPERTIES : EXPOSED -----------------------------------------------
		public ObservableCollection<ShopItem> Shops
		{
			get => _shops;
			set => SetProperty(ref _shops, value);
		}



		// CONSTRUCTOR --------------------------------------------------------
		public ShopListViewModel()
		{
			// commandes
			SelectedCommand = new Command<ShopItem>(SelectedAction);
			GoUserSettings = new Command(GoUserSettingsAction);
			GoCommandValidation = new Command(GoCommandValidationAction);
		}



		// COMMANDES : IMPLEMENTATION -----------------------------------------
		private void GoPizzaListAction(object shop)
		{
			INavigationService navigationService = DependencyService.Get<INavigationService>();
			navigationService.PushAsync<PizzaListPage>(
				new Dictionary<string, object>()
				{
					{ "Shop", (ShopItem)shop }
				}
			);
		}


		private void SelectedAction(ShopItem shop)
		{
			GoPizzaListAction(shop);
		}


		private void GoCommandValidationAction()
		{
			INavigationService navigationService = DependencyService.Get<INavigationService>();
			navigationService.PushAsync<CommandValidationPage>();
		}


		private void GoUserSettingsAction()
		{
			INavigationService navigationService = DependencyService.Get<INavigationService>();
			navigationService.PushAsync<UserSettingsPage>();
		}



		// EVENEMENTS ---------------------------------------------------------
		public override async Task OnResume()
		{
            await base.OnResume();
			IPizzaApiService service = DependencyService.Get<IPizzaApiService>();
			IOAuth2Service auth2 = DependencyService.Get<IOAuth2Service>();
			if (auth2.CheckExpired())
			{
				RefreshRequest refreshRequest = auth2.getRefreshRequest();
				Response<LoginResponse> response1 = await service.Refresh(refreshRequest);
				if (response1.IsSuccess)
				{
					auth2.SaveToken(response1.Data);
				}
			}

			var locator = CrossGeolocator.Current;
			var position = await locator.GetPositionAsync();
			Location location = new Location(position.Latitude, position.Longitude);
			// met � jour la liste des magasins
			Response<List<ShopItem>> response = await service.ListShops();
			
			
			if (response.IsSuccess) {
				List<ShopItem> l = new List<ShopItem>(response.Data);

				// Distance entre le magazin et la position de l'utilisateur
				for (int index = 0; index < l.Count; index++)
				{
					ShopItem shop = l[index];
					Location destination = new Location(shop.Latitude, shop.Longitude);
					l[index].MinutesPerKilometer = (int)Location.CalculateDistance(location, destination, DistanceUnits.Miles);
				}

					l.Sort((firstObj, secondObj) =>{
					return firstObj.MinutesPerKilometer.CompareTo(secondObj.MinutesPerKilometer);
				});

				Shops = new ObservableCollection<ShopItem>(l); 
			}
		}
	}
}