using PizzaIllico.Mobile.Dtos;
using PizzaIllico.Mobile.Dtos.Accounts;
using PizzaIllico.Mobile.Dtos.Authentications.Credentials;
using PizzaIllico.Mobile.Services;
using PizzaIllico.Mobile.Pages;
using Storm.Mvvm;
using Storm.Mvvm.Services;
using System;
using System.Windows.Input;
using Xamarin.Forms;
using Plugin.Toast;
using PizzaIllico.Mobile.Dtos.Authentications;

namespace PizzaIllico.Mobile.ViewModels
{
	class UserSettingsViewModel : ViewModelBase
	{
		// PROPERTIES : PRIVATE -----------------------------------------------
		private SetUserProfileRequest _setUserProfileRequest;
		private UserProfileResponse _userProfileResponse;
		private SetPasswordRequest _setPasswordRequest;
		private bool _showProfileForm;
		private bool _showProfileMenu;
		private bool _editProfileForm;
		private bool _editPassword;



		// COMMANDES : DEFINITION ---------------------------------------------
		public ICommand BackUserSettings { get; }
		public ICommand GoCommandValidation { get; }
		public ICommand GoCommandHistory{ get; }
		public ICommand UpdateUserCommand { get; }
		public ICommand UpdateNewPassWordCommand { get; }
		public ICommand UndoCommand { get; }
		public ICommand GoModifyProfileCommand { get; }
		public ICommand GoModifyPasswordCommand { get; }



		// PROPERTIES : EXPOSED -----------------------------------------------
		public SetUserProfileRequest SetUserProfileRequest
		{
			get => _setUserProfileRequest;
			set => SetProperty(ref _setUserProfileRequest, value);
		}

		public UserProfileResponse UserProfileResponse
		{
			get => _userProfileResponse;
			set => SetProperty(ref _userProfileResponse, value);
		}

		public SetPasswordRequest SetPasswordRequest
		{
			get => _setPasswordRequest;
			set => SetProperty(ref _setPasswordRequest, value);
		}

		public bool ShowProfileForm
		{
			get => _showProfileForm;
			set => SetProperty(ref _showProfileForm, value);
		}

		public bool ShowProfileMenu
		{
			get => _showProfileMenu;
			set => SetProperty(ref _showProfileMenu, value);
		}

		public bool EditProfileForm
		{
			get => _editProfileForm;
			set => SetProperty(ref _editProfileForm, value);
		}

		public bool EditPassword
		{
			get => _editPassword;
			set => SetProperty(ref _editPassword, value);
		}



		// CONSTRUCTOR --------------------------------------------------------
		public UserSettingsViewModel()
		{
			_setPasswordRequest = new SetPasswordRequest();

			ConfigurationInit();
			
			BackUserSettings = new Command(BackAction);
			GoCommandValidation = new Command(GoCommandValidationAction);
			GoCommandHistory = new Command(GoCommandHistoryAction);
			UpdateUserCommand = new Command(UpdateUserAction);
			UpdateNewPassWordCommand = new Command(UpdateNewPassWordAction);
			UndoCommand = new Command(() => ConfigurationInit());
			GoModifyProfileCommand = new Command(() => ConfigurationModifyProfile());
			GoModifyPasswordCommand = new Command(() => ConfigurationModifyPassword());
		}



		// COMMANDES : IMPLEMENTATION -----------------------------------------
		public async void UpdateNewPassWordAction()
		{
			if (SetPasswordRequest.NewPassword != "" && SetPasswordRequest.OldPassword != "")
			{
				IPizzaApiService service = DependencyService.Get<IPizzaApiService>();
				Response response = await service.UpdatePassWord(SetPasswordRequest);

				if (response.IsSuccess)
				{
					ConfigurationInit();
					Console.WriteLine($"Appel HTTP : {response.IsSuccess}");
					CrossToastPopUp.Current.ShowToastMessage("Sucess");
				}
				else
				{
					switch(response.ErrorCode)
					{
						case "INVALID_PASSWORD":
							CrossToastPopUp.Current.ShowToastMessage("Error: invalid password");
							break;

						case "WEAK_PASSWORD":
							CrossToastPopUp.Current.ShowToastMessage("Error: weak password");
							break;

						default:
							CrossToastPopUp.Current.ShowToastMessage("Error");
							break;
					}
				}
			}
			else
			{
				CrossToastPopUp.Current.ShowToastMessage("Error : empty field");
			}
		}


		public async void UpdateUserAction()
		{
			SetUserProfileRequest = new SetUserProfileRequest();
			SetUserProfileRequest.Email = UserProfileResponse.Email;
			SetUserProfileRequest.FirstName = UserProfileResponse.FirstName;
			SetUserProfileRequest.LastName = UserProfileResponse.LastName;
			SetUserProfileRequest.PhoneNumber = UserProfileResponse.PhoneNumber;

			if (SetUserProfileRequest.Email != "" && SetUserProfileRequest.FirstName != "" && SetUserProfileRequest.LastName != "" && SetUserProfileRequest.PhoneNumber != "")
			{

				IPizzaApiService service = DependencyService.Get<IPizzaApiService>();
				Response<UserProfileResponse> response = await service.UpdateUser(SetUserProfileRequest);

				if (response.IsSuccess)
				{
					UserProfileResponse = response.Data;
					ConfigurationInit();
					CrossToastPopUp.Current.ShowToastMessage("Sucess");
				}
				else
				{
					//add error code
					CrossToastPopUp.Current.ShowToastMessage("Error");
				}
			}
			else
			{
				CrossToastPopUp.Current.ShowToastMessage("Error : empty field");
			}
		}


		private void GoCommandHistoryAction(object obj)
		{
			INavigationService navigationService = DependencyService.Get<INavigationService>();
			navigationService.PushAsync<CommandHistoryPage>();
		}


		private void GoCommandValidationAction(object obj)
		{
			INavigationService navigationService = DependencyService.Get<INavigationService>();
			navigationService.PushAsync<CommandValidationPage>();
		}


		private void BackAction(object obj)
		{
			INavigationService navigationService = DependencyService.Get<INavigationService>();
			navigationService.PopAsync();
		}



		// FUNCTIONS ----------------------------------------------------------
		private void ConfigurationInit()
		{
			ShowProfileForm = true;
			ShowProfileMenu = true;
			EditProfileForm = false;
			EditPassword = false;
		}


		private void ConfigurationModifyProfile()
		{
			ShowProfileForm = true;
			ShowProfileMenu = false;
			EditProfileForm = true;
			EditPassword = false;
		}


		private void ConfigurationModifyPassword()
		{
			ShowProfileForm = false;
			ShowProfileMenu = false;
			EditProfileForm = false;
			EditPassword = true;
		}


		public async void GetUer()
		{
			IPizzaApiService service = DependencyService.Get<IPizzaApiService>();
			IOAuth2Service auth2 = DependencyService.Get<IOAuth2Service>();
			if (auth2.CheckExpired())
			{
				RefreshRequest refreshRequest = auth2.getRefreshRequest();
				Response<LoginResponse> response1 = await service.Refresh(refreshRequest);
				if (response1.IsSuccess)
				{
					auth2.SaveToken(response1.Data);
				}
			}
			Response<UserProfileResponse> response = await service.GetUser();
			if (response.IsSuccess)
			{
				UserProfileResponse = response.Data;
			} 
		}
	}
}
